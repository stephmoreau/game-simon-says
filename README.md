## Simon Says Style Game ##
Using only Html and CSS to create the board for the game. 
JS to be added later to create a more dynamic game.

---

## Extra files used
List of files besides out index.html and style.css
1. Google Fonts to make it look a little nicer


---

## JavaScript
JavaScript will be included for the following:
1. Randomize the board
2. Count score
3. Sound effects

---

## Future Tasks ##
Notes on possible options/functionality that could be added

1. Nothing yet
